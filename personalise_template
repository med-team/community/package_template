#!/bin/sh -e
# Replaces templates for developer name, source package and data for you

if [ ! -f /usr/bin/realpath ]; then
  echo "E: Please install the coreutils package for 'realpath'."
  exit 1
fi

rootdir=$(realpath $(dirname "$0"))

if [ "package_template" = "$(basename $(dirname $(pwd)))" ]; then
  echo "E: Execute this script from with in the root of a new package's source tree."
  exit 1
fi

if [ -d debian ]; then
  echo "E: The current directory is the destination directory. It should not already have a debian directory."
  exit 1
fi

if [ "$DEBEMAIL" = "" ] ; then
  echo "E: Please set DEBEMAIL in your environment for instance in .bashrc"
  exit 1
fi

if [ "$DEBFULLNAME" = "" ] ; then
  echo "E: Please set DEBFULLNAME in your environment for instance in .bashrc"
  exit 1
fi

if [ $# -lt 1 ] ; then
  echo "usage: $0 <sourcepackagename>"
  echo "       $0 <homepage>"
  exit 1
fi

if echo "$1" | grep -q '^http.*://' ; then
  PHOMEPAGE="$1"
  PKG=$(echo "$1" | sed 's#^.*/\([^/]\+\)/*$#\1#' | tr '[:upper:]' '[:lower:]' | tr '_' '-' )
  echo PKG="$PKG"
else
  PHOMEPAGE=""
  PKG=$1
fi

if ! mkdir "$PKG" ; then
  echo "E: Unable to create dir \"$PKG\"."
  exit 1
fi

# FIXME: Check whether a package with this source name exists on salsa!

CURDIR=$(pwd)
cp -av $(dirname "$0")/debian "$PKG"
cd "$PKG"/debian
sed -i "s/<your_ID>/${DEBFULLNAME} <${DEBEMAIL}>/" $(find . -maxdepth 1 -type f)
sed -i "s/<pkg>/$PKG/" $(find . -maxdepth 1 -type f)
CHDATE=$(date -R)
sed -i "s/<timestamp_as_per_date_-R>/${CHDATE}/" changelog
if [ "$PHOMEPAGE" != "" ] ; then
  sed -i "s#<homepage>#$PHOMEPAGE#" control
fi

if echo "$PHOMEPAGE" | grep -q 'github\.com' ; then
  echo "version=4\n" > watch
  echo "opts=\"filenamemangle=s%(?:.*?)?v?(\d[\d.]*)\.tar\.gz%@PACKAGE@-\$1.tar.gz%\" \\" >> watch
  echo "$PHOMEPAGE/tags (?:.*?/)?v?@ANY_VERSION@\.tar\.gz" >> watch
fi
if echo "$PHOMEPAGE" | grep -q 'gitlab\.com' ; then
  echo "version=4\n" > watch
  UNAME=$(echo $PHOMEPAGE | sed 's:/$::' | sed 's:^.*/\([^/]\+\):\1:')
  # use \.tar\.gz instead of @ARCHIVE_EXT@ since we'll end up with bz2 archive instead of tar.gz
  echo "$PHOMEPAGE/tags?sort=updated_desc .*/archive/v?@ANY_VERSION@/${UNAME}[-v]*.*\.tar\.gz" >> watch
  sed -i -e "s/^\(Upstream-Name:\).*/\1 ${UNAME}/" -e "s?^\(Source:\).*?\1 ${PHOMEPAGE}?" copyright
fi

STANDARDSVERSION=$(LC_ALL=C apt-cache policy debian-policy | grep Candidate | sed 's/^ *Candidate: \([0-9]\.[0-9]\.[0-9]\).*/\1/')
if [ "$STANDARDSVERSION" = "" ] ; then
  echo "W: Failure detecting latest Standards-Version"
else
  sed -i "s/^Standards-Version:.*/Standards-Version: $STANDARDSVERSION/" control
fi

cd "$CURDIR"
mv "$PKG" "$PKG"_
